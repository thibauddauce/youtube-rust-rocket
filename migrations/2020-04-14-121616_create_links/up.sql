CREATE TABLE links (
    id INTEGER PRIMARY KEY NOT NULL,
    url TEXT NOT NULL,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    name TEXT,
    category TEXT NOT NULL
);