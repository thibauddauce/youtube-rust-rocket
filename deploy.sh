rsync --progress database/seed.sql hadibut.fr:/var/www/rocket.hadibut.fr/database
rsync --progress -r static hadibut.fr:/var/www/rocket.hadibut.fr
rsync --progress Rocket.toml hadibut.fr:/var/www/rocket.hadibut.fr

cargo build --release
rsync --progress target/release/hello-rocket hadibut.fr:/var/www/rocket.hadibut.fr