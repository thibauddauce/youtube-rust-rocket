table! {
    links (id) {
        id -> Integer,
        url -> Text,
        title -> Text,
        description -> Text,
        name -> Nullable<Text>,
        category -> Text,
    }
}
