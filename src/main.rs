#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_migrations;

use rocket::Rocket;
use rocket::fairing::AdHoc;
use rocket::request::Form;
use rocket::response::Redirect;
use rocket_contrib::databases::diesel::SqliteConnection;
use rocket_contrib::serve::StaticFiles;
use rocket::response::Flash;
use rocket::request::FlashMessage;
use askama::Template;
use diesel::prelude::*;
use std::collections::HashMap;

pub mod schema;

use self::schema::links::dsl::{links as all_links, name as link_name};

embed_migrations!();

#[derive(Template)]
#[template(path = "index.html")]
struct HomePageTemplate {
    links_by_category: HashMap<String, Vec<Link>>,
    flash_message: Option<(i32, String)>,
}

#[database("db")]
struct DbConn(SqliteConnection);

#[derive(Queryable, Debug)]
struct Link {
    id: i32,
    url: String,
    title: String,
    description: String,
    name: Option<String>,
    category: String,
}

#[derive(Debug, FromForm)]
struct TakeLink {
    name: String,
}

#[get("/")]
fn index(conn: DbConn, flash: Option<FlashMessage>) -> HomePageTemplate {
    let links = all_links.load::<Link>(&*conn).unwrap();

    let mut links_by_category = HashMap::new();
    for link in links {
        let links = links_by_category.entry(link.category.clone()).or_insert(vec![]);
        links.push(link);
    }

    HomePageTemplate { 
        links_by_category: links_by_category,
        flash_message: flash.map(|flash| {
            let mut parts = flash.msg().split(":");
            let link_id: i32 = parts.next().unwrap().parse().unwrap();
            let msg = parts.next().unwrap().to_string();
            (link_id, msg)
        })
    }
}

#[post("/take/<link_id>", data = "<form>")]
fn take_link(conn: DbConn, form: Form<TakeLink>, link_id: i32) -> Flash<Redirect> {
    diesel::update(all_links.find(link_id))
        .set(link_name.eq(&form.name))
        .execute(&*conn)
        .unwrap();

    Flash::success(Redirect::to(uri!(index)), format!("{}:{}", link_id, "Bien enregistré."))
}

fn run_db_migrations(rocket: Rocket) -> Result<Rocket, Rocket> {
    let conn = DbConn::get_one(&rocket).unwrap();
    embedded_migrations::run(&*conn).unwrap();

    Ok(rocket)
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, take_link])
        .mount("/static", StaticFiles::from("./static"))
        .attach(DbConn::fairing())
        .attach(AdHoc::on_attach("Database Migrations", run_db_migrations))
        .launch();
}